import java.util.Timer;
import java.util.TimerTask;

public class StockSimulator {
    public static void main(String[] args) {
        TimerTask timerTask = new MyTimerTask();
        Timer timer = new Timer(true);
        //Запуск іде кожні 10 секунд (10 * 1000 мілісекунд)
        timer.scheduleAtFixedRate(timerTask, 0, 10*1000);
        try {
            Thread.sleep(240000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        timer.cancel();
        System.out.println("The work is complete");
    }

}