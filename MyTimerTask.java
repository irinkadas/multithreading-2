import java.util.Date;
import java.util.TimerTask;
public class MyTimerTask extends TimerTask {
    @Override
    public  void run() {
        System.out.println("Running threads: " + new Date());
        Thread t1 = new Thread((Runnable) new UpdatePrice(), "t1");
        Thread t2 = new Thread((Runnable) new Targets(), "t2");
        Thread t3 = new Thread((Runnable) new StocksInformation(),"t3");
        t1.start();
        try {
            t1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        t2.start();
        try {
            t2.join();
        }catch (InterruptedException i){
            i.printStackTrace();
        }
        t3.start();
    }
}