import java.time.LocalTime;
public class Targets implements Runnable{
    @Override
    public void run() {
        targetToBuyAlice1();
        targetToBuyAlice2();
        targetToBuyBob1();
        targetToBuyBob2();
        targetToBuyCharlie1();
        failedTargetAlice1();
        failedTargetAlice2();
        failedTargetBob1();
        failedTargetBob2();
        failedTargetCharlie1();
    }
    public static synchronized void targetToBuyAlice1(){
        if (AAPL.priceAAPL<=100){
            try {
                Thread.sleep(5000);
                for (int i = 0; i < 1; i++){
                    System.out.println(LocalTime.now() + " Alice's attempt to buy AAPL stock is successful. 10 shares were bought.");
                    System.out.println("AAPL shares left: "+ (AAPL.amountAAPL - AliceAAPL.amountAAPL) );}
            } catch (InterruptedException e) {
                throw new RuntimeException(e);}
        }
    }
    public static synchronized void targetToBuyAlice2() {
        if (COKE.priceCOKE >= 390) {
            try {
                Thread.sleep(5000);
                for (int i = 0; i < 1; i++) {
                    System.out.println(LocalTime.now() + " Alice's attempt to buy COKE stock is successful. 20 shares were bought.");
                    System.out.println("COKE shares left: " + (COKE.amountCOKE - AliceCOKE.amountCOKE));}
            } catch (InterruptedException e) {
                throw new RuntimeException(e);}
        }
    }
    public static synchronized void targetToBuyBob1(){
        if (AAPL.priceAAPL<=140){
            try {
                Thread.sleep(5000);
                for (int i = 0; i < 1; i++){
                    System.out.println(LocalTime.now() + " Bob attempt to buy AAPL stock is successful. 10 shares were bought.");
                    System.out.println("AAPL shares left: " + (AAPL.amountAAPL - BobAAPL.amountAAPL));}
            }catch (InterruptedException e) {
                throw new RuntimeException(e);}
        }
    }
    public static synchronized void targetToBuyBob2(){
        if (IBM.priceIMB<=135){
            try {
                Thread.sleep(5000);
                for (int i = 0; i < 1; i++){
                    System.out.println(LocalTime.now() + " Bob attempt to buy IBM stock is successful.20 shares were bought.");
                    System.out.println("IBM shares left: " + (IBM.amountIMB - BobIBM.amountIBM));}
            }catch (InterruptedException e) {
                throw new RuntimeException(e);}
        }
    }
    public static synchronized void targetToBuyCharlie1() {
        if (COKE.priceCOKE <= 370) {
            try {
                Thread.sleep(5000);
                for (int i = 0; i < 1; i++) {
                    System.out.println(LocalTime.now() + " Charlie attempt to buy COKE stock is successful. 300 shares were bought.");
                    System.out.println("COKE shares left: " + (COKE.amountCOKE - CharlieCOKE.amountCOKE));
                }
            }catch (InterruptedException e) {
                throw new RuntimeException(e);}
        }
    }
    public static synchronized void failedTargetAlice1(){
        if (AAPL.priceAAPL>=100){
            try {
                Thread.sleep(5000);
                for (int i = 0; i < 1; i++){
                    System.out.println(LocalTime.now() + " Alice attempt to buy AAPL stock is unsuccessful");}
            }catch (Exception l){
                l.printStackTrace();
            }
        }
    }
    public static synchronized void failedTargetAlice2(){
        if (COKE.priceCOKE<=390){
            try {
                Thread.sleep(5000);
                for (int i = 0; i < 1; i++){
                    System.out.println(LocalTime.now() + " Alice attempt to buy COKE stock is unsuccessful");}
            }catch (Exception l){
                l.printStackTrace();
            }
        }
    }
    public static synchronized void failedTargetBob1(){
        if (AAPL.priceAAPL>=140){
            try {
                Thread.sleep(5000);
                for (int i = 0; i < 1; i++){
                    System.out.println(LocalTime.now() + " Bob attempt to buy AAPL stock is unsuccessful");}
            }catch (Exception l){
                l.printStackTrace();
            }
        }
    }
    public static synchronized void failedTargetBob2(){
        if (IBM.priceIMB>=135){
            try {
                Thread.sleep(5000);
                for (int i = 0; i < 1; i++){
                    System.out.println(LocalTime.now() + " Bob attempt to buy IBM stock is unsuccessful");}
            }catch (Exception l){
                l.printStackTrace();
            }
        }
    }
    public static synchronized void failedTargetCharlie1(){
        if (COKE.priceCOKE>=370){
            try {
                Thread.sleep(5000);
                for (int i = 0; i < 1; i++){
                    System.out.println(LocalTime.now() + " Charlie attempt to buy COKE stock is unsuccessful");}
            }catch (Exception l){
                l.printStackTrace();
            }
        }
    }
}
