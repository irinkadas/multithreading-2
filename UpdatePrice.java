import java.util.concurrent.ThreadLocalRandom;
public class UpdatePrice implements Runnable{
    @Override
    public void run() {
        updatePrice();
    }
    public static double plusMinusInt(double n, int percent) {
        int p = ThreadLocalRandom.current().nextInt(2 * percent + 1) - percent;
        return n * (100.0 + p) / 100.0;
    }
    public static synchronized void updatePrice() {
        try {
            Thread.sleep(10000);
            for (int i = 0; i < 1; i++) {
                AAPL.priceAAPL = plusMinusInt(AAPL.priceAAPL, 3);
                IBM.priceIMB = plusMinusInt(IBM.priceIMB, 3);
                COKE.priceCOKE = plusMinusInt(COKE.priceCOKE, 3);
                System.out.println("The price AAPL has changed: " + AAPL.priceAAPL);
                System.out.println("The price COkE has changed: " + COKE.priceCOKE);
                System.out.println("The price IBM has changed: " + IBM.priceIMB);
            }
        } catch (Exception r) {
            r.printStackTrace();
        }
    }
}

